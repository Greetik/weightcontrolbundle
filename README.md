# README #


### What is this repository for? ###

* Weightcontrol is a bundle to store weights of an item in different dates

### How do I get set up? ###

Just install it via composer and you can add a service in your app to check the permissions before make a upload or edit a file.

##Add it to AppKernel.php.##
new \Greetik\WeightcontrolBundle\WeightcontrolBundle()


##In the config.yml you can add your own service##
blog:
    permsservice: app.weightcontrol
