<?php

namespace Greetik\WeightcontrolBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Weightcontrol
 *
 * @ORM\Table(name="weightcontrol", indexes={
 *      @ORM\Index(name="itemtype", columns={"itemtype"}),  @ORM\Index(name="itemid", columns={"itemid"}),  @ORM\Index(name="item", columns={"itemid", "itemtype"})
 * })
 * @ORM\Entity(repositoryClass="Greetik\WeightcontrolBundle\Repository\WeightcontrolRepository")
 */
class Weightcontrol
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var float
     *
     * @ORM\Column(name="weight", type="float")
     */
    private $weight;

    /**
     * @var \Datetime
     *
     * @ORM\Column(name="weightdate", type="date")
     */
    private $weightdate;
    
    /**
     * @var string
     *
     * @ORM\Column(name="itemtype", type="string", length=255)
     */
    private $itemtype;

    /**
     * @var integer
     *
     * @ORM\Column(name="itemid", type="integer")
     */
    private $itemid;

   
/**
     * @var string
     *
     * @ORM\Column(name="extra", type="string", length=255, nullable=true)
     */
    private $extra;

    
    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set weight
     *
     * @param float $weight
     *
     * @return Weightcontrol
     */
    public function setWeight($weight)
    {
        $this->weight = $weight;

        return $this;
    }

    /**
     * Get weight
     *
     * @return float
     */
    public function getWeight()
    {
        return $this->weight;
    }

    /**
     * Set weightdate
     *
     * @param \DateTime $weightdate
     *
     * @return Weightcontrol
     */
    public function setWeightdate($weightdate)
    {
        $this->weightdate = $weightdate;

        return $this;
    }

    /**
     * Get weightdate
     *
     * @return \DateTime
     */
    public function getWeightdate()
    {
        return $this->weightdate;
    }

    /**
     * Set itemtype
     *
     * @param string $itemtype
     *
     * @return Weightcontrol
     */
    public function setItemtype($itemtype)
    {
        $this->itemtype = $itemtype;

        return $this;
    }

    /**
     * Get itemtype
     *
     * @return string
     */
    public function getItemtype()
    {
        return $this->itemtype;
    }

    /**
     * Set itemid
     *
     * @param integer $itemid
     *
     * @return Weightcontrol
     */
    public function setItemid($itemid)
    {
        $this->itemid = $itemid;

        return $this;
    }

    /**
     * Get itemid
     *
     * @return integer
     */
    public function getItemid()
    {
        return $this->itemid;
    }

    /**
     * Set extra
     *
     * @param string $extra
     *
     * @return Weightcontrol
     */
    public function setExtra($extra)
    {
        $this->extra = $extra;

        return $this;
    }

    /**
     * Get extra
     *
     * @return string
     */
    public function getExtra()
    {
        return $this->extra;
    }
}
