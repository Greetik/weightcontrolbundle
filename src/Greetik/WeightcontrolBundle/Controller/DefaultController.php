<?php

namespace Greetik\WeightcontrolBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Greetik\WeightcontrolBundle\Entity\Weightcontrol;
use Greetik\WeightcontrolBundle\Form\Type\WeightcontrolType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class DefaultController extends Controller
{
    /**
    * Show the new link insert form
    * 
    * @author Pacolmg
    */
     public function insertformAction($item_id, $item_type, $id='')
     {
        
         if (!empty($id)){
             $weightcontrol=$this->get('weightcontrol.tools')->getWeightcontrolObject($id);
             $item_type = $weightcontrol->getItemtype();
             $item_id = $weightcontrol->getItemid();
         }
         else{
            $weightcontrol=new Weightcontrol();
         }
         
         $newForm = $this->createForm(WeightcontrolType::class, $weightcontrol);

        return $this->render($this->getParameter('weightcontrol.interface').':insert.html.twig',array('new_form' => $newForm->createView(), 'item_id' => $item_id, 'item_type'=>$item_type,'id' =>$id, 'item'=>$this->get($this->getParameter('weightcontrol.permsservice'))->getWeightcontrolItem($item_id, $item_type)));
    }

    /**
    * Edit the data of an weightcontrol or insert a new one
    * 
    * @param int $id is received by Get Request
    * @param Weightcontrol $item is received by Post Request
    * @author Pacolmg
    */
     public function insertmodifyAction(Request $request, $item_id, $item_type, $id=''){
        $item = $request->get('weightcontrol');
      
        if (!empty($id)){
            $weightcontrol=$this->get('weightcontrol.tools')->getWeightcontrolObject($id);
            $editing = true;
        }else{ $weightcontrol = new Weightcontrol(); $editing=false;}

         $editForm = $this->createForm(WeightcontrolType::class, $weightcontrol);
         $editForm->handleRequest($request);
        
        if ($editForm->isValid()) {
            if ($editing){
                try{
                    $this->get($this->getParameter('weightcontrol.permsservice'))->modifyWeightcontrol($weightcontrol);
                }catch(\Exception $e){
                    return new Response(json_encode(array('errorCode'=>1, 'errorDescription'=>$e->getMessage())), 200, array('Content-Type'=>'application/json'));
                }
            }else{
                try{
                    $weightcontrol->setItemtype($item_type);
                    $weightcontrol->setItemid($item_id);
                    //$weightcontrol->setNumorder(0);
                    $this->get($this->getParameter('weightcontrol.permsservice'))->insertWeightcontrol($weightcontrol);
                }catch(\Exception $e){
                    return new Response(json_encode(array('errorCode'=>1, 'errorDescription'=>$e->getMessage())), 200, array('Content-Type'=>'application/json'));
                }                
            }
            
            return $this->render($this->getParameter('weightcontrol.interface').':index.html.twig', array('data' => $this->get($this->getParameter('weightcontrol.permsservice'))->getWeightcontrolByItem($weightcontrol->getItemid(), $weightcontrol->getItemtype()), '_itemid'=>$weightcontrol->getItemid(), '_itemtype'=>$weightcontrol->getItemtype(), 'modifyAllow'=>true));
        }else{
            $errors = $editForm->getErrorsAsString();
            return new Response(json_encode(array('errorCode'=>1, 'errorDescription'=>$errors)), 200, array('Content-Type'=>'application/json'));
        }
         return new Response(json_encode(array('errorCode'=>1, 'errorDescription'=>'Error Desconocido')), 200, array('Content-Type'=>'application/json'));
     }

     /**
    * Delete a weightcontrol
    * 
    * @param int $id is received by Get Request
    * @author Pacolmg
    */
     public function dropAction($id)
     {
        $weightcontrol = $this->get('weightcontrol.tools')->getWeightcontrolObject($id);
        
       try{
        $this->get($this->getParameter('weightcontrol.permsservice'))->deleteWeightcontrol($weightcontrol);
       }catch(\Exception $e){
           return new Response(json_encode(array('errorCode'=>1, 'errorDescription'=>$e->getMessage())), 200, array('Content-Type'=>'application/json'));
       }
       
        return $this->render($this->getParameter('weightcontrol.interface').':index.html.twig', array('data' => $this->get($this->getParameter('weightcontrol.permsservice'))->getWeightcontrolByItem($weightcontrol->getItemid(), $weightcontrol->getItemtype()), '_itemid'=>$weightcontrol->getItemid(), '_itemtype'=>$weightcontrol->getItemtype(), 'modifyAllow'=>true));
     }

}
