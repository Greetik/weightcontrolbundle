<?php

namespace Greetik\WeightcontrolBundle\Services;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Weightcontrol Tools
 *
 * @author Pacolmg
 */
class Weightcontrol {

    private $em;
    private $beinterface;

    public function __construct($_entityManager, $_beinterface) {
        $this->em = $_entityManager;
        $this->beinterface= $_beinterface;
    }

   
    public function getWeightcontrolByItem($item_id, $item_type) {
        return $this->em->getRepository('WeightcontrolBundle:Weightcontrol')->getWeightcontrolByItem($item_id, $item_type);
    }

    public function getWeightcontrolObject($id) {
            return $this->em->getRepository('WeightcontrolBundle:Weightcontrol')->findOneById($id);
    }

    public function getWeightcontrol($id) {
            return $this->em->getRepository('WeightcontrolBundle:Weightcontrol')->getWeightcontrol($id);
    }

    public function getWeightcontrolByExtra($item_id, $item_type, $extra) {
            return $this->em->getRepository('WeightcontrolBundle:Weightcontrol')->getWeightcontrolByExtra($item_id, $item_type, $extra);
    }

    public function getWeightcontrolByDates($item_type, $from, $to, $ids='', $start='', $length=''){
        return $this->em->getRepository('WeightcontrolBundle:Weightcontrol')->getWeightcontrolByDates($item_type, $from, $to, $ids, $start, $length);
    }

    public function getWeightcontrolByDatesNum($item_type, $from, $to, $ids=''){
        return $this->em->getRepository('WeightcontrolBundle:Weightcontrol')->getWeightcontrolByDatesNum($item_type, $from, $to, $ids);
    }

    public function modifyWeightcontrol($weightcontrol) {
        $this->em->persist($weightcontrol);
        $this->em->flush();
    }

    public function insertWeightcontrol($weightcontrol) {
        $this->em->persist($weightcontrol);
        $this->em->flush();
    }

    public function deleteWeightcontrol($weightcontrol) {
        if (is_numeric($weightcontrol)) $weightcontrol = $this->getWeightcontrolObject ($weightcontrol);
        $this->em->remove($weightcontrol);
        $this->em->flush();
    }
}