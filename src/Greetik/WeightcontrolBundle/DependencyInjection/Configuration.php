<?php

namespace Greetik\WeightcontrolBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * This is the class that validates and merges configuration from your app/config files.
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/configuration.html}
 */
class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritdoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('weightcontrol');

        $rootNode
            ->children()
                ->scalarNode('permsservice')->defaultValue('weightcontrol.tools')->end()
                ->scalarNode('interface')->defaultValue('WeightcontrolBundle:Default')->end()
            ->end()
        ;

        return $treeBuilder;
    }
}
